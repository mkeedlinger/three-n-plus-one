use clap::{App, Arg};
use dashmap::DashMap;
use im::Vector;
use rug::{Complete, Integer};
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex};
use std::thread::spawn;
use std::time::Instant;

struct IntGen {
    cur: Mutex<Integer>,
    max_len: AtomicU64,
    up_to: Integer,
}

impl IntGen {
    fn new(up_to: Integer) -> Self {
        IntGen {
            cur: Mutex::new(Integer::from(1)),
            max_len: AtomicU64::new(0),
            up_to,
        }
    }
    fn set_up_to(&mut self, up_to: Integer) {
        self.up_to = up_to;
    }
    fn next(&self) -> Option<Integer> {
        let mut int = self.cur.lock().unwrap();

        if *int > self.up_to {
            None
        } else {
            let n = int.clone();
            *int += 1;
            Some(n)
        }
    }
}

fn main() {
    let matches = App::new("3n+1")
        .arg(Arg::with_name("threads").short("t").takes_value(true))
        .arg(
            Arg::with_name("to-index")
                .short("i")
                .takes_value(true)
                .required(true),
        )
        .get_matches();
    let mut threads = vec![];

    let start = Instant::now();

    let threads_num = {
        let t: usize = matches
            .value_of("threads")
            .unwrap_or("0")
            .parse()
            .unwrap_or(0);
        if t == 0 {
            num_cpus::get()
        } else {
            t
        }
    };

    let mut ints = IntGen::new(Integer::from(0));

    {
        let up_to = matches.value_of("to-index").unwrap();
        let up_to = Integer::parse_radix(up_to, 10)
            .expect("Failed to parse")
            .complete();
        ints.set_up_to(up_to);
    }

    let map: Arc<DashMap<Integer, Vector<Integer>>> =
        Arc::new(DashMap::with_capacity(ints.up_to.to_usize().unwrap()));

    let ints = Arc::new(ints);
    for _ in 0..threads_num {
        let ints = ints.clone();
        let map = map.clone();
        threads.push(spawn(move || {
            while let Some(next_int) = ints.next() {
                let len = traverse(next_int, &map);

                if ints.max_len.fetch_max(len, Ordering::SeqCst) < len {
                    dbg!(len);
                }
            }
        }))
    }

    for t in threads.drain(..) {
        t.join().unwrap();
    }

    let end = Instant::now();

    dbg!(&*map.get(&ints.up_to).unwrap());

    println!();

    println!("Up to int: {}", ints.up_to);

    println!("Computation took {} seconds", (end - start).as_secs());

    println!(
        "Largest line found: {}",
        ints.max_len.load(Ordering::Relaxed)
    );
}

fn traverse(start: Integer, map: &Arc<DashMap<Integer, Vector<Integer>>>) -> u64 {
    let mut len = 1;
    let mut v = Vector::new();
    let mut cur = start.clone();

    loop {
        v.push_back(cur.clone());

        if cur == 1 {
            break;
        }

        len += 1;
        if cur.is_even() {
            cur /= 2;
        } else {
            cur *= 3;
            cur += 1;
        }

        if map.contains_key(&cur) {
            let cached = map.get(&cur).unwrap().clone();
            v.append(cached);
            // println!("Was able to cache from {} at {}", start, cur);
            break;
        }
    }
    map.insert(start, v);
    len
}
